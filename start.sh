#!/bin/sh

docker run \
    --name ikev2-vpn-server \
    -p 500:500/udp \
    -p 4500:4500/udp \
    -p 80:80 \
    -v /lib/modules:/lib/modules:ro \
    --cap-add=NET_ADMIN \
    --device="/dev/ppp:/dev/ppp" \
    --sysctl net.ipv4.ip_forward=1 \
    # --sysctl net.ipv4.conf.all.accept_redirects=0 \
    # --sysctl net.ipv4.conf.all.send_redirects=0 \
    # --sysctl net.ipv4.conf.all.rp_filter=0 \
    # --sysctl net.ipv4.conf.default.accept_redirects=0 \
    # --sysctl net.ipv4.conf.default.send_redirects=0 \
    # --sysctl net.ipv4.conf.default.rp_filter=0 \
    # --sysctl net.ipv4.conf.eth0.send_redirects=0 \
    # --sysctl net.ipv4.conf.eth0.rp_filter=0 \
    --sysctl net.ipv6.conf.all.forwarding=1 \
    --sysctl net.ipv6.conf.eth0.proxy_ndp=1 \
    -v "$PWD/data/certs/certs:/usr/local/etc/ipsec.d/certs" \
    -v "$PWD/data/certs/private:/usr/local/etc/ipsec.d/private" \
    -v "$PWD/data/certs/cacerts:/usr/local/etc/ipsec.d/cacerts" \
    -v "$PWD/data/etc/ipsec.d/ipsec.secrets:/usr/local/etc/ipsec.secrets" \
    --env-file "$PWD/.env" \
    -v /lib/modules:/lib/modules:ro \
    -d --privileged \
    --restart=always \
    drzhnin/docker-ikev2-vpn-server
